```
_____                 
___(_)______ _______ _
__  /__  __ `/_  __ `/
_  / _  /_/ /_  /_/ / 
/_/  _\__, / _\__, /
     /____/  /____/

igg, version 0.1
```

## General Description

In the spirit of the classic UNIX utility `dig`, `igg` (the IP geolocation groper) is a command line utility for geolocating IP addresses and host names. Written in Python 3, and limited to modules from the standard library, `igg`'s core functionality is to either retrieve either (1) a complete geolocation record for a given IPv4 address (or each IPv4 address to which a given host name resolves), or (2) specific data elements from that IP address's (or a host's IPs) records.

The utility operates by executing `urllib` requests  against the [IP Location API](http://ip-api.com). The utility makes use of the free version of the API, which allows for 150 requests per minute originating from a single IP address.<sup>[1](#myfootnote1)</sup> This makes the utility more suitable for *ad hoc* queries, than for heavy batch processing. Although, the utility's output can easily be piped into dependent scripts.

The idea for this utility came from the built-in `dig` and `whois` UNIX programs, both of which enable investigations into open source intelligence (OSINT) data relating to IP addresses from the command line. This utility fills a gap that exists between these two other programs, providing fast access to geolocation data for any valid external IP address straight from the command line.

**NOTE:** *This Python utility represents a ground-up reworking of the now depricated `ipgeo` [utility](https://gitlab.com/nxl4/ipgeo) previously written in `bash`. `igg` includes added functionality, and utilizes a more robust API.*

## Input

Data is input into the utility via command line arguments. There are two categories of arguments accepted by the utility: (1) one positional argument, and (2) three optional arguments.

### Positional Argument

The single positional argument defines either the IP address or host name to be used in the query.

### Optional Arguments

There are a total of three optional arguments accepted by the utility:

* `-h, --help` Shows Help Message and Exits
* `-f, --format` Defines Response Format
* `-e, --element` Defines Response Data Element

The `help` argument is the standard Python `argparse` command. The `format` and `element` arguments are mutually exclusive; i.e. you can *either* define the response format, *or* the specific data element retrieved. 

The typical intended use of the utility does not require either argument, and will print a structured version of the data to the console. The `format` argument is designed for use cases where the data is needed for another dependent process in a particular format (e.g. JSON). The `element` argument is designed for use cases where a single string-formatted data element (e.g. country code) is needed for another dependent process.

#### Response Format Options

There are a total of four pre-defined response format options:

* `json` JavaScript Object Notation
* `xml` Extensible Markup Language
* `line` Newline
* `csv` Comma Separated Values

#### Data Element Options

There are a total of eighteen pre-defined data element options:

* `status` Success or Failure (e.g. success)
* `country` Country Name (e.g. United States)
* `countryCode` Two-Letter Country Code, ISO 3166-1 Alpha-2 (e.g. US)
* `region` Region/State Short Code (FIPS or ISO) (e.g. CA)
* `regionName` Region/State (e.g. California)
* `city` City (e.g. Mountain View
* `zip` Zip Code (e.g. 94043)
* `lat` Latitude (e.g. 37.4192)
* `lon` Longitude (e.g. -122.0574)
* `timezone` City Timezone (e.g. America/Los_Angeles)
* `isp` ISP Name (e.g. Google)
* `org` Organization Name (e.g. Google)
* `as` AS Number and Name (e.g. AS15169 Google Inc.)
* `reverse` Reverse DNS of the IP (e.g. wi-in-f94.1e100.net)
* `mobile` Mobile (Cellular) Connection (e.g. true)
* `proxy` Proxy (Anonymous) (e.g. true)
* `query` IP Used for the Query (e.g. 173.194.67.94)

## Output

Depending on which, if any, optional arguments are provided, there are three general formats in which the utility will present output: (1) default, (2) format specific, and (3) element specific.

### Default

The default output is presented as a list of all of the data elements for each queried IP address, in key value format. For example, see the default output for `173.194.67.94`:

![out_1](img/out_1.png)

For default queries where the result is a failure, the following three fields will be presented in the same key value format. For example, see the default output for the bad IP value `173.194.67.941`:

![out_1e](img/out_1e.png)

### Format Specific

The format specific output is presented in the specific format defined in the `--format` argument's option. For example, see the `xml` formatted output for `173.194.67.94`:

![out_2](img/out_2.png)

For format specific queries where the result is a failure, the following three fields will be presented in the same specified format. For example, see the format specified output for the bad IP value `173.194.67.941` in `xml` format:

![out_2e](img/out_2e.png)

### Element Specific

The element specific output presents a string containing the specific data element defined in the `--element` argument's option. For example, see the `country` output for `173.194.67.94`:

![out_3](img/out_3.png)

For element specific queries where the result is a failure, the following three fields will be presented in the default format. For example, see the default for the bad IP value `173.194.67.941` with a `country` element specified:

![out_3e](img/out_3e.png)

## Usage

The general usage structure of the utility is:

```shell
igg [ -h ] [ -f { json, xml, line, csv } | -e { country, countryCode, region, regionName, city, zip, lat, lon, timezone, isp, org, as, reverse, mobile, proxy, query, status, message } ] ip_addr
```

### Default Usage

To retrieve default formatted results, the only argument necessary is the single positional argument. For an example querying an IP address:

```shell
igg 2.2.2.5
```

For an example querying a host name:

```shell
igg example.com
```

### Format Specific Usage

To retrieve format specified results, pass the positional argument defining either the IP address or host name, along with the `-f, --format` argument and one of the four pre-defined options. For an example querying an IP address:

```shell
igg 2.2.2.5 --format xml
```

For an example querying a host name:

```shell
igg example.com -f json
```

### Element Specific Usage

To retrieve element specified results, pass the positional argument defining either the IP address or host name, along with the `-e, --element` argument and one of the eighteen pre-defined options. For an example querying an IP address:

```shell
igg 2.2.2.5 --element isp
```

For an example querying a host name:

```shell
igg example.com -e proxy
```

## Installation

### UNIX-like

The easiest way to install this utility is to first clone the repository:

```shell
git clone https://gitlab.com/nxl4/ipgeo.git
```

And then to run the `make` command on the `makefile` from within the cloned repository's new directory:

```shell
sudo make install
```

Likewise, the `make` command can be used to uninstall the utility:

```shell
sudo make uninstall
```

### Windows

**TODO:** *Develop Windows-specific installation script.*

I've tested the utility in a Windows 10 environment, but have not yet developed any script similar to the POSIX makefile described above.

## Issues

Please report any issues to the [Issue Tracker](https://gitlab.com/nxl4/igg/issues/new) for this repository. 

## Author

nxl4: [nxl4@protonmail.com](nxl4@protonmail.com)

## License

[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Notes
<a name="myfootnote1">1</a>: More on this limit from the [API's documentation](http://ip-api.com):

>The limit is 150 requests per minute from an IP address. If you go over this limit your IP address will be blackholed. You can unban [here](http://ip-api.com/docs/unban).

I will also note here that in testing this utility, the only way I was able to intentionally get my IP address blackholded was to concurrently operate 4 terminals, with a `while` loop in each, cycling through repeated hits to the API with the utility.
