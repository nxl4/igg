PREFIX ?= /usr

all:
	@echo Run \'make install\' to install igg.

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@cp -p igg.py $(DESTDIR)$(PREFIX)/bin/igg
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/igg

uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/bin/igg
