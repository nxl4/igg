#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""igg

In the spirit of the classic UNIX utility dig, igg 
(IP geolocation groper) is a command line utility for 
geolocating IP addresses and domains.

Date:
    9 December 2018

Version:
    0.1

License:
    GPL v3.0

Authors:
    nxl4@protonmail.com

"""

__email_addr__  = 'nxl4@protonmail.com'
__gitlab_addr__ = 'https://gitlab.com/nxl4/igg'
__version__     = '0.1'

import argparse
import json
import os
import re
import socket
import urllib.error
import urllib.request as req

FIELD_LIST = ['country', 'countryCode',
              'region', 'regionName',
              'city', 'zip',
              'lat', 'lon',
              'timezone', 'isp',
              'org', 'as',
              'reverse', 'mobile',
              'proxy', 'query',
              'status', 'message']

def arg_parser():
    """Argument parser

    Defines argument parser for CLI script. The parser has one
    required positional argument: the IP address or domain to
    be queried. There are two mutually exclusive optional
    arguments: (1) the response format, and (2) the data
    element. Each optional argument has a pre-defined list
    of available options.
    
    Returns:
        This function returns an argument namespace object
        with three elements: (1) a list of any input IP address
        string values, (2) a list of any format string values,
        and (3) a list of any data element string values.
    """
    # init arg parser
    parser = argparse.ArgumentParser(
        description = "igg, {0} ({1}): "
        "IP geolocation groper".format(
            __version__, 
            __gitlab_addr__
            )
        )
    # add positional IP argument
    parser.add_argument(
        "ip_addr",
        action = "store",
        nargs = 1,
        type = str,
        help = "IP address"
        )
    # create mutually exclusive group
    group = parser.add_mutually_exclusive_group(required=False)
    # add response format to group
    group.add_argument(
        "-f", 
        "--format",
        action = "store",
        choices = ['json',
                   'xml',
                   'line',
                   'csv'],
        nargs = 1,
        type = str,
        help = "full response format",
        dest = "rsp_format",
        required = False
        )
    # modify list of available fields
    selectable_fields = FIELD_LIST.copy()
    selectable_fields.remove('message')
    # add response element to group
    group.add_argument(
        "-e", 
        "--element",
        action = "store",
        choices = selectable_fields,
        nargs = 1,
        type = str,
        help = "single response element",
        dest = "rsp_element",
        required = False
        )
    args = parser.parse_args()
    return args


def url_opener(url, raw_input):
    """
    URL opener

    This function handles opening the constructed URLs needed
    to communicate with the API and query IP addresses.

    Args:
        url: A string representing a URL.
        
        raw_input: A string representing the operation's query
        term (i.e. IP address or domain).

    Returns:
        This function returns a string representation of the
        data structure was requested by the URL submitted to
        the API (e.g. JSON, XML), containing the geolocation
        data for the queried IP address, or (in the event of
        a failed query) the failure reason returned by the API.

    Raises:
        SystemExit: Exit code 1 represents a failure of the 
        utility due to an inability to open the API's URL.
    """
    env_proxy = os.environ.get('HTTP_PROXY')
    try:
        # connect through web proxy
        if env_proxy is not None:
            proxy = req.ProxyHandler({'http': env_proxy})
            auth = req.HTTPBasicAuthHandler()
            opener = req.build_opener(
                proxy, 
                auth, 
                req.HTTPHandler
                )
            req.install_opener(opener)
            conn = req.urlopen(url)
            rsp = conn.read().decode()
        # proxyless connections
        else:
            conn = req.urlopen(url)
            rsp = conn.read().decode()
        return rsp
    ### TODO: identify other possible urlopen() errors
    ###       and write necessary exceptions for function
    except urllib.error.URLError as e:
        print('\n{0: <10}{1}'.
            format('Query:', raw_input))
        print('{0: <10}{1}'.
            format('Status:', 'fail'))
        print('{0: <10}{1}\n'.
            format(
                'Message:', 
                'Temporary failure in name resolution'
                ', check internet connection'))
        raise SystemExit(1)


def url_constructor(arg_dict):
    """URL constructor

    This function handles constructing the URLs needed
    to communicate with the API and query IP addresses.

    Args:
        arg_dict: A dictionary containing (1) the query value,
        and any optional (2) format requirements, or (3) data
        element requirements.

    Returns:
        This string representing a URL.
    """
    url_root = "http://ip-api.com/"
    # casts field list to string
    all_fields = ",".join(FIELD_LIST)
    # requests with defined format requirements
    if arg_dict['rsp_format'] is not None:
        url = "{0}{1}/{2}?fields={3}".format(
            url_root,
            arg_dict['rsp_format'],
            arg_dict['ip_addr'],
            all_fields
            )
    # requests with defaults
    else:
        url = "{0}{1}/{2}?fields={3}".format(
            url_root,
            'json',
            arg_dict['ip_addr'],
            all_fields
            )
    return url


def rsp_evaluator(arg_dict, rsp):
    """Response evaluator

    This function evaluates the response returned by the API.
    Depending on the structure of the request, and the result
    of the API's response, either an error message, or some
    type of structured data is printed to the console.

    Args:
        arg_dict: A dictionary with the following structure:

            { 'raw_input': <str>,
              'rsp_element': <str>,
              'rsp_format': <str> }

        rsp: a string representation of the data structure was 
        requested by the URL submitted to the API (e.g. JSON, 
        XML), containing the geolocation data for the queried 
        IP address, or (in the event of a failed query) the 
        failure reason returned by the API.

    """
    rsp_element = arg_dict['rsp_element']
    rsp_format = arg_dict['rsp_format']
    # handles responses with defined elements
    if rsp_element is not None:
        rsp_dict = json.loads(rsp)
        # handles successes
        if rsp_dict['status'] == 'success':
            print(rsp_dict[rsp_element])
        # handles failures
        elif rsp_dict['status'] == 'fail':
            print('\n{0: <10}{1}'.
                format('Query:',rsp_dict['query']))
            print('{0: <10}{1}'.
                format('Status:',rsp_dict['status']))
            print('{0: <10}{1}\n'.
                format('Message:',rsp_dict['message']))
    # handles responses with defined formats
    elif rsp_format is not None:
        print(rsp)
    # handles default request formats and elements
    else:
        rsp_dict = json.loads(rsp)
        # populates blank fields
        for field in FIELD_LIST:
            if field in rsp_dict:
                if rsp_dict[field] == '':
                    rsp_dict[field] = 'n/a'
                else:
                    pass
            else:
                rsp_dict[field] = 'n/a'
        # handles successes
        if rsp_dict['status'] == 'success':
            print('\n{0: <20}{1}'.
                format('Query:',rsp_dict['query']))
            print('{0: <20}{1}'.
                format('Status:',rsp_dict['status']))
            print('{0: <20}{1}'.
                format('Autonomous System:',rsp_dict['as']))
            print('{0: <20}{1}'.
                format('ISP:',rsp_dict['isp']))
            print('{0: <20}{1}'.
                format('Organization:',rsp_dict['org']))
            print('{0: <20}{1}'.
                format('Country:',rsp_dict['country']))
            print('{0: <20}{1}'.
                format('Country Code:',rsp_dict['countryCode']))
            print('{0: <20}{1}'.
                format('Region Name:',rsp_dict['regionName']))
            print('{0: <20}{1}'.
                format('Region Code:',rsp_dict['region']))
            print('{0: <20}{1}'.
                format('City:',rsp_dict['city']))
            print('{0: <20}{1}'.
                format('ZIP Code:',rsp_dict['zip']))
            print('{0: <20}{1}'.
                format('Latitude:',rsp_dict['lat']))
            print('{0: <20}{1}'.
                format('Longitude:',rsp_dict['lon']))
            print('{0: <20}{1}'.
                format('Timezone:',rsp_dict['timezone']))
            print('{0: <20}{1}'.
                format('Proxy (Anonymous):',rsp_dict['proxy']))
            print('{0: <20}{1}'.
                format('Mobile Connection:',rsp_dict['mobile']))
            print('{0: <20}{1}\n'.
                format(
                    'Reverse DNS of IP:',
                    rsp_dict['reverse']
                    ))
        # handles failures
        elif rsp_dict['status'] == 'fail':
            print('\n{0: <10}{1}'.
                format('Query:',rsp_dict['query']))
            print('{0: <10}{1}'.
                format('Status:',rsp_dict['status']))
            print('{0: <10}{1}\n'.
                format('Message:',rsp_dict['message']))


def eval_input(ip_addr):
    """Input evaluator

    This function accepts a string value as input, and tests
    it with two regex patterns, to determine if it is struct-
    urally capable of being either an IP address or a host
    name.

    Args:
        ip_addr: A string representation of a value that may
        be either an IP address or host name.

    Returns:
        This function returns a dictionary with one of the three
        following structured, depending on whether the input
        is an IP address, a host, or neither:

            { 'type': <string>,
              'ip_addr': <list> }

            { 'type': <string>,
              'host': <list> }

            { 'type': None }
    """
    input_eval = {}
    # IP pattern evaluation
    ip_regex = (
        "^[0-9]{1,3}[.][0-9]{1,3}[.]"
        "[0-9]{1,3}[.][0-9]{1,3}$"
        )
    ip_pattern = re.compile(ip_regex)
    ip_match = re.findall(ip_pattern, ip_addr)
    # host name pattern evaluation
    host_regex = (
        "[a-zA-Z0-9][a-zA-Z0-9-.]*"
        "[a-zA-Z0-9]\.[a-zA-Z]{2,}$"
        )
    host_pattern = re.compile(host_regex)
    host_match = re.findall(host_pattern, ip_addr)
    # handles IP matches
    if len(ip_match) >= 1:
        input_eval['type'] = 'IP'
        input_eval['ip_addr'] = ip_match
    # handles host name matches
    elif len(host_match) >= 1:
        input_eval['type'] = 'Host'
        input_eval['host'] = host_match
    # handles invalid input
    else:
        input_eval['type'] = None
    return input_eval


def get_dns_info(host):
    """Get DNS info

    This function resolves a given host name into a list of
    IP addresses.

    Args:
        host: A string representing the host name.

    Raises:
        SystemExit: Exit code 2 represents a failure due to
        a domain name that cannot be found. Exit code 3 rep-
        resents a failure due to a failure in the name reso-
        lution service (typically caused by an outage of
        internet service). Exit code 4 represents a previously
        undefined socket error.
    """
    try:
        # resolves host name to IP address list
        raw_info = socket.gethostbyname_ex(host)
        ip_list = raw_info[2]
        return ip_list
    except socket.gaierror as e:
        # define socket errors
        error_2 = "Name or service not known"
        error_3 = "Temporary failure in name resolution"
        # create error messages and codes
        if error_2 in str(e):
            error_msg = error_2
            exit_code = 2
        elif error_3 in str(e):
            error_msg = error_3
            exit_code = 3
        else:
            error_msg = str(e)
            exit_code = 4
        # handle specific socket errors
        print('\n{0: <10}{1}'.
            format('Query:', host))
        print('{0: <10}{1}'.
            format('Status:', 'fail'))
        print('{0: <10}{1}\n'.
            format('Message:', error_msg))
        raise SystemExit(exit_code)


def input_handler(arg_dict):
    """Input handler

    This function handles input arguments. It determines if the
    raw input is either an IP address or a host name. IP
    addesses are handled with one branch. In the other branch,
    hosts are resolved into IP addresses, if possible, which
    are subsequently handled in series.

    Args:
        arg_dict: A dictionary with the following structure:

            { 'raw_input': <str>,
              'rsp_element': <str>,
              'rsp_format': <str> }

    Raises:
        SystemExit: Exit code 5 represents a failure due to
        an unresolvable domain. Exit code 6 represents a 
        failure due to an invalid input that's neither an IP
        nor a domain.
    """
    # determines if raw input is IP or host
    input_eval = eval_input(arg_dict['raw_input'])
    # handles IP address inputs
    if input_eval['type'] == 'IP':
        arg_dict['ip_addr'] = input_eval['ip_addr'][0]
        url = url_constructor(arg_dict)
        rsp = url_opener(url, arg_dict['ip_addr'])
        rsp_evaluator(arg_dict, rsp)
        raise SystemExit(0)
    # handles host name inputs
    elif input_eval['type'] == 'Host':
        # retrieves DNS info from host name
        dns_info = get_dns_info(input_eval['host'][0])
        # handles host IP addresses
        if len(dns_info) >= 1:
            for ip in dns_info:
                arg_dict['ip_addr'] = ip
                url = url_constructor(arg_dict)
                rsp = url_opener(url, arg_dict['ip_addr'])
                rsp_evaluator(arg_dict, rsp)
            raise SystemExit(0)
        # handles unresolvable host inputs
        else:
            print('\n{0: <10}{1}'.
                format('Query:',arg_dict['raw_input']))
            print('{0: <10}{1}'.
                format('Status:','fail'))
            print('{0: <10}{1}\n'.
                format('Message:','Unresolvable domain'))
            raise SystemExit(5)
    # handles invalid (non-IP and non-host) inputs
    elif input_eval['type'] == None:
        print('\n{0: <10}{1}'.
            format('Query:',arg_dict['raw_input']))
        print('{0: <10}{1}'.
            format('Status:','fail'))
        print('{0: <10}{1}\n'.
            format('Message:','invalid query'))
        raise SystemExit(6)


def main():
    """Main utility function

    This function parses the CLI arguments, and creates a
    new dictionary based on the argument options. This 
    dictionary is passed to the input handler, that kicks 
    off the rest of the utility's functions.
    """
    args = arg_parser()
    # restructure arguments as dictionary
    arg_dict = {}
    # handle IP and domain values
    if type(args.ip_addr) == list:
        arg_dict['raw_input'] = args.ip_addr[0]
    elif type(args.ip_addr) == str:
        arg_dict['raw_input'] = args.ip_addr
    # handle response element values
    if type(args.rsp_element) == list:
        arg_dict['rsp_element'] = args.rsp_element[0]
    elif type(args.rsp_element) == str:
        arg_dict['rsp_element'] = args.rsp_element
    else:
        arg_dict['rsp_element'] = None
    # handle response format values
    if type(args.rsp_format) == list:
        arg_dict['rsp_format'] = args.rsp_format[0]
    elif type(args.rsp_format) == str:
        arg_dict['rsp_format'] = args.rsp_format
    else:
        arg_dict['rsp_format'] = None
    # starts utility function
    input_handler(arg_dict)


if __name__ == '__main__':
    main()
